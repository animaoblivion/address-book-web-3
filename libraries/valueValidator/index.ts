import Rules from "@/libraries/valueValidator/rules";
import { useState } from "react";

interface ValueValidator {
  formConfig: any;
  fieldName: string;
  fieldValue: string;
}

const Index = ({ formConfig, fieldName, fieldValue }: ValueValidator) => {
  let validationResponse: Array<string> = [];
  const rules = formConfig[fieldName].rule;
  rules.map((rule: string) => {
    const ruleName = rule as keyof typeof Rules;
    const validationFunction = Rules[ruleName];
    const isValidationFail = validationFunction({ value: fieldValue });
    if (isValidationFail) validationResponse.push(isValidationFail);
  });
  return validationResponse;
};

export default Index;
