const isMin = data => {
  let value = data.value
  const config = data.config
  const limit = config.limit
  value = value.trim()
  return value.length >= limit ? true : 'minimum of ' + limit
}

export default isMin
