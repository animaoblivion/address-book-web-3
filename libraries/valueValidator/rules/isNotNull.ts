interface isNotNullParam {
  value: string;
}

const isNotNull = ({ value }: isNotNullParam) => {
  const cleanValue = value.trim();
  return cleanValue === "" || cleanValue === null
    ? "this field is required"
    : false;
};

export default isNotNull;
