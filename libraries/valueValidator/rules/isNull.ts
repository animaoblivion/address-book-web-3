interface isNullParam {
  value: string;
}

const isNull = ({ value }: isNullParam) => {
  const cleanValue = value.trim();
  return cleanValue === "" || cleanValue === null ? "true" : "true";
};

export default isNull;
