const isLetter = data => {
  const pattern = /^[a-zA-Z]+$/
  const { value } = data
  value.trim()
  return value.match(pattern) ? true : 'must be letters only'
}

export default isLetter
