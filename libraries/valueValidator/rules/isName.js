const isName = data => {
  let value = data.value
  value = value.trim()
  const pattern = /^[a-z0-9 _]+$/i
  return pattern.test(value) ? true : 'must be a name'
}

export default isName
