import internal from "stream";

const Index = async (
  array: Array<any>,
  callback: (indexObj: any, idx?: string | number, arr?: Array<any>) => void,
) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};
export default Index;
