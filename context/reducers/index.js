const Index = (state, action) => {
  switch (action.type) {
    case "SET_TOKEN":
      return {
        ...state,
        token: action.payload,
      };
    case "SET_ACCOUNT":
      return {
        ...state,
        account: action.payload,
      };
    case "SET_LOOKUP":
      return {
        ...state,
        lookup: action.payload,
      };
    case "RESET":
      return {};
    default:
      return state;
  }
};

export default Index;
