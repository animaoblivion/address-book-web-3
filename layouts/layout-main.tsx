interface Props {
  children: JSX.Element | JSX.Element[];
}

const Index: React.FC<Props> = ({ children }) => {
  return <div className="md:container md:mx-auto m-auto">{children}</div>;
};

export default Index;
