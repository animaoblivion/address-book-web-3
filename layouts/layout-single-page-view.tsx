interface Props {
  children: JSX.Element | JSX.Element[];
}

const Index: React.FC<Props> = ({ children }) => {
  return <>{children}</>;
};

export default Index;
