import { useContext, useEffect } from "react";

import ContextApp from "@/context/index";

const Index: React.FC = () => {
  const { state, dispatch } = useContext(ContextApp.Context);
  const { me } = state;

  return (
    <div>
      <h1>Header</h1>
    </div>
  );
};

export default Index;
