import { useContext, useEffect } from "react";
import Router from "next/router";
import { useRouter } from "next/router";

import ContextApp from "@/context/index";
import Layout_Main from "@/layouts/layout-main";

interface Props {
  children: JSX.Element | JSX.Element[];
}

const Index: React.FC<Props> = ({ children }) => {
  const { state, dispatch } = useContext(ContextApp.Context);

  useEffect(() => {
    console.log(state);
  }, []);

  return <Layout_Main>{children}</Layout_Main>;
};

export default Index;
