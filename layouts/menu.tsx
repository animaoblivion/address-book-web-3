import { useContext, useEffect } from "react";
import Router from "next/router";
import Link from "next/link";
import { useRouter } from "next/router";

import ContextApp from "@/context/index";

const Index: React.FC = () => {
  const { state, dispatch } = useContext(ContextApp.Context);
  const router = useRouter();
  const isLoggedIn = state.me ? true : false;

  const handleLogout = () => {
    // logout();
    router.push("/login");
  };

  return (
    <div>
      <p>
        <Link href="/">
          <a>
            <b>Home</b>
          </a>
        </Link>
      </p>
      <p>
        logout
      </p>
    </div>
  );
};

export default Index;
