interface Props {
  children: JSX.Element | JSX.Element[];
}

const Index: React.FC<Props> = ({ children }) => {
  return (
    <>
      <div className="grid grid-cols-4 gap-4">
        <div className="col-span-2">
          Head
        </div>
        <div className="col-span-2">
          Menu
        </div>
        <div className="col-span-4">
          Other
        </div>
        <div className="col-span-1">
          Other
        </div>
        <div className="col-span-3">
          children components
        </div>
        <div className="col-span-4">
          footer
        </div>
      </div>
    </>
  );
};

export default Index;
