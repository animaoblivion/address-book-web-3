import React from "react";

interface Props {
  formConfig: {};
  children: JSX.Element | JSX.Element[];
}

const Index: React.FC<Props> = ({ formConfig, children }) => {
  console.log(formConfig, " CONFIG");
  return <>{children}</>;
};

export default Index;
