import React from "react";

interface Props {
  placeholder?: string;
  error: Array<string>;
  name: string;
  value: string;
  className?: string | null | undefined;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Index: React.FC<Props> = ({
  placeholder,
  error,
  name,
  value,
  className,
  onChange,
}) => {
  const defaultStyleValid =
    "bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500";
  const defaultStyleInValid =
    "bg-red-200 appearance-none border-2 border-red-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-red-500";
  let defaultStyle = defaultStyleValid;
  if (className) defaultStyle = className;
  if (error.length > 0) defaultStyle = defaultStyleInValid;

  return (
    <input
      placeholder={placeholder}
      type="text"
      id={name}
      name={name}
      value={value}
      onChange={onChange}
      className={defaultStyle}
    />
  );
};

export default Index;
