import React from "react";

interface Label {
  name: string;
  label: string;
}

interface Props {
  label: Label;
  className?: string | null | undefined;
}

const Index: React.FC<Props> = ({ label, className }) => {
  let defaultStyle = "block text-gray-700 text-sm font-bold mb-2";
  if (className) defaultStyle = className;
  return (
    <label
      className="block text-gray-700 text-sm font-bold mb-3 mt-5"
      htmlFor={label.name}
    >
      {label.label}
    </label>
  );
};

export default Index;
