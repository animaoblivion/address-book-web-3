import React from "react";

interface Props {
  label: string;
  className?: string | null | undefined;
}

const Index: React.FC<Props> = ({ label, className }) => {
  let defaultStyle =
    "py-2 px-4 bg-green-500 text-white font-semibold rounded-lg shadow-md hover:bg-green-700 focus:outline-none";
  if (className) defaultStyle = className;

  return (
    <button type="submit" className={defaultStyle}>
      {label}
    </button>
  );
};

export default Index;
