import React from "react";

interface Props {
  label: Array<string>;
  className?: string | null | undefined;
}

const Index: React.FC<Props> = ({ label, className }) => {
  let defaultStyle = "py-1 pl-1 text-sm text-red-400";
  if (className) defaultStyle = className;

  return (
    <>
      {label.map((text, index) => (
        <p key={index} className={defaultStyle}>
          {text}
        </p>
      ))}
    </>
  );
};

export default Index;
