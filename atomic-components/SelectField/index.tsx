import React from "react";

interface OptionProps {
  value: string;
  label: string;
}

interface Props {
  error: Array<string>;
  name: string;
  options: OptionProps[];
  className?: string | null | undefined;
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

const Index: React.FC<Props> = ({
  error,
  name,
  options,
  className,
  onChange,
}) => {
  const defaultStyleValid =
    "bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500";
  const defaultStyleInValid =
    "bg-red-200 appearance-none border-2 border-red-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-red-500";
  let defaultStyle = defaultStyleValid;
  if (className) defaultStyle = className;
  if (error.length > 0) defaultStyle = defaultStyleInValid;

  return (
    <select id={name} name={name} onChange={onChange} className={defaultStyle}>
      {options.map((data, index) => (
        <option key={index} value={data.value}>
          {data.label}
        </option>
      ))}
    </select>
  );
};

export default Index;
