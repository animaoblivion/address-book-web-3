import React from "react";

interface Props {
  label: string;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}

const Index: React.FC<Props> = ({ label, onClick }) => {
  return (
    <button type="submit" onClick={onClick}>
      {label}
    </button>
  );
};

export default Index;
