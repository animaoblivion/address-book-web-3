import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import MainForm from "./main-form";

interface Props {
  address: string;
}

interface ObjectKeyVal {
  [key: string]: any;
}

const Index: React.FC<Props> = ({ address }) => {
  // start: form handle action
  const handleSubmit = async ({ formData }: ObjectKeyVal): Promise<void> => {
    console.log(address);
    return;
  };
  // end: form handle action
  return <MainForm handleSubmit={handleSubmit} />;
};

export default Index;
