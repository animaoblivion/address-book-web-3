import React, { useEffect, useState } from "react";
import Link from "next/link";

import SelectField from "@/atomic-components/SelectField";
import TextField from "@/atomic-components/TextField";
import PasswordField from "@/atomic-components/PasswordField";
import TextFieldLabel from "@/atomic-components/TextFieldLabel";
import TextFieldErrorLabel from "@/atomic-components/TextFieldErrorLabel";
import SubmitButton from "@/atomic-components/SubmitButton";
import Button from "@/components/misc/button";

import useFormWrapp from "@/hooks/useFormWrapp";
import useFormValidation from "@/hooks/useFormValidation";

import ValueValidator from "@/libraries/valueValidator";

interface ObjectKeyVal {
  [key: string]: any;
}

interface Props {
  handleSubmit: ({ formData }: ObjectKeyVal) => Promise<void>;
}

interface HandleChangeParams {
  name: string;
  value: string;
}

const Index: React.FC<Props> = ({ handleSubmit }) => {
  const formConfig = {
    value: {
      label: "value",
      value: "",
      rule: ["isNotNull"],
      error: [],
    },
  };
  const { SetNewValidation } = useFormValidation({
    formConfig,
  });
  const {
    formHasError,
    formLabel,
    formData,
    formError,
    setNewData,
    setNewError,
    setBulkError,
  } = useFormWrapp({
    formConfig,
  });

  // ----------------
  // : onChange
  // # form input changes
  const onChange = ({ name, value }: HandleChangeParams) => {
    // validation
    const key = name as keyof typeof formConfig;
    const validationValue = ValueValidator({
      formConfig,
      fieldName: name,
      fieldValue: value,
    });
    setNewData({ fieldName: name, fieldValue: value });
    setNewError({ fieldName: name, fieldValue: validationValue });
  };

  // ----------------
  // : onSubmit
  // # form submit changes
  const onSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();

    // all field validation
    let preparedError: ObjectKeyVal = {};
    Object.keys(formData).map((fieldName) => {
      const key = fieldName as keyof typeof formConfig;
      // validation
      const validationValue = SetNewValidation({
        fieldName,
        fieldValue: formData[key],
      });
      // setNewData({ fieldName, fieldValue: formData[fieldName] });
      // setNewError({ fieldName, fieldValue: validationValue });
      preparedError[fieldName] = validationValue;
    });
    setBulkError(preparedError);

    // ----------------------
    // form has error
    // return nothing
    const isError =
      Object.keys(preparedError)
        .map((f) => preparedError[f].length)
        .reduce((a, b) => a + b) > 0
        ? true
        : false;
    if (isError) return;
    if (formHasError) return;

    // ----------------------
    // call handleSubmit
    return handleSubmit({ formData });
  };

  return (
    <form onSubmit={onSubmit}>
      {/* start: section */}
      <TextField
        placeholder={formLabel.value.label}
        error={formError.value}
        name="value"
        value={formData.value}
        onChange={(e) => {
          onChange({ name: "value", value: e.target.value });
        }}
      />
      <TextFieldErrorLabel label={formError.value} />
      <br />
      {/* end */}

      {/* start: section */}
      <br />
      <Button text="Send" />
      {/* end */}
    </form>
  );
};

export default Index;
