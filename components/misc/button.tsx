import { useEffect, useState } from "react";

import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

interface ButtonParams {
  text: string;
  twcss?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

const MainButton: React.FC<ButtonParams> = ({ text, twcss, onClick }) => {
  return (
    <button
      onClick={onClick}
      className={
        "w-full shadow focus:shadow-outline focus:outline-none text-white font-bold py-1 px-4 rounded-full " +
        twcss
      }
      type="submit"
    >
      {text}
    </button>
  );
};

const Button: React.FC<ButtonParams> = ({ text, onClick }) => {
  return (
    <>
      <MainButton
        onClick={onClick}
        text={text}
        twcss="bg-yellow-500	 hover:bg-yellow-400"
      />
    </>
  );
};

export const ButtonBlue: React.FC<ButtonParams> = ({ text, onClick }) => {
  return (
    <>
      <MainButton
        onClick={onClick}
        text={text}
        twcss="bg-blue-500 hover:bg-blue-400"
      />
    </>
  );
};

export const ButtonTrans: React.FC<ButtonParams> = ({ text, onClick }) => {
  return (
    <>
      <MainButton
        onClick={onClick}
        text={text}
        twcss="bg-red-500 hover:bg-red-400"
      />
    </>
  );
};

export default Button;
