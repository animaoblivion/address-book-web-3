import Link from "next/link";

interface Props {
  backLink: string;
  pageTitle: string;
  action: ActionProps;
}

interface ActionProps {
  actionLink?: string;
  actionText?: string;
}

const Action: React.FC<ActionProps> = ({ actionLink, actionText }) => {
  if (!actionLink || !actionText) return null;
  return (
    <Link href={actionLink}>
      <a>
        <small>{actionText}</small>
      </a>
    </Link>
  );
};

const Option: React.FC<Props> = ({ backLink, pageTitle, action }) => {
  return (
    <div className="flex justify-around">
      <div className="w-full flex justify-start">
        <Link href={backLink}>
          <a>
            <small>{"<"}</small>
          </a>
        </Link>
      </div>
      <div className="w-full flex justify-center">
        <p>
          <small>{pageTitle}</small>
        </p>
      </div>
      <div className="w-full flex justify-end">
        <Action actionLink={action.actionLink} actionText={action.actionText} />
      </div>
    </div>
  );
};

export default Option;
