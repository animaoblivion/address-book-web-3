import { useEffect, useState } from "react";

import Avatar, { AvatarBig } from "@/components/contact/avatar";
import Address from "@/components/contact/address";

interface ItemParams {
  name: string;
  address: string;
  initial: string;
}

const ContactItem: React.FC<ItemParams> = ({ name, address, initial }) => {
  return (
    <div>
      <div className="flex justify-center py-2">
        <AvatarBig text={initial} />
      </div>
      <div className="flex flex-wrap content-center py-2">
        <Address text={address} />
      </div>
    </div>
  );
};

export default ContactItem;
