import { useEffect, useState } from "react";

import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

interface NameParams {
  text: string;
  twcss?: string;
}
const Name = ({ text }: NameParams) => {
  return (
    <div className="h-8 pl-2 flex flex-wrap justify-center content-center">
      {text}
    </div>
  );
};

export default Name;
