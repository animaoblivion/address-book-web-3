import { useEffect, useState } from "react";

import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

interface AvatarParams {
  text: string;
  twcss?: string;
}
const MainAvatar: React.FC<AvatarParams> = ({ text, twcss }) => {
  return (
    <div
      className={
        "bg-gray-500 rounded-full flex flex-wrap justify-center content-center font-bold " +
        twcss
      }
    >
      {text}
    </div>
  );
};

const Avatar: React.FC<AvatarParams> = ({ text }) => {
  return <MainAvatar text={text} twcss="w-8 h-8" />;
};

export const AvatarBig: React.FC<AvatarParams> = ({ text }) => {
  return <MainAvatar text={text} twcss="w-20 h-20" />;
};

export default Avatar;
