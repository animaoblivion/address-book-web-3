import { useEffect, useState } from "react";

import Avatar from "@/components/contact/avatar";
import Name from "@/components/contact/name";

interface ItemParams {
  name: string;
  initial: string;
}

const ContactItem: React.FC<ItemParams> = ({ name, initial }) => {
  return (
    <div className="flex flex-wrap content-start py-2">
      <Avatar text={initial} />
      <Name text={name} />
    </div>
  );
};

export default ContactItem;
