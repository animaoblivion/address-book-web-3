import { useEffect, useState } from "react";

import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

interface FeeParams {
  fee: number;
  twcss?: string;
}
const Fee = ({ fee }: FeeParams) => {
  return (
    <div className="w-full pl-2 flex flex-wrap justify-center content-center">
      {fee}
    </div>
  );
};

export default Fee;
