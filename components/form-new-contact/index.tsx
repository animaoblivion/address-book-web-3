import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import MainForm from "./main-form";
import useAddressBook from "@/hooks/useAddressBook";

interface ObjectKeyVal {
  [key: string]: any;
}

const Index: React.FC = () => {
  // start: form handle action
  const router = useRouter();
  const { addAddress } = useAddressBook();
  const handleSubmit = async ({ formData }: ObjectKeyVal): Promise<void> => {
    console.log(formData, " formData");
    addAddress(formData);
    router.push("/");
    return;
  };
  // end: form handle action
  return <MainForm handleSubmit={handleSubmit} />;
};

export default Index;
