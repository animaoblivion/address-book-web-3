import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import MainForm from "./main-form";
import Button from "@/components/misc/button";
import useAddressBook from "@/hooks/useAddressBook";

interface ObjectKeyVal {
  [key: string]: any;
}

const Index: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const { updateAddress, removeAddress, addressBookContact } = useAddressBook();
  const profile = addressBookContact[Number(id)];

  // start: form handle action
  const handleSubmit = async ({ formData }: ObjectKeyVal): Promise<void> => {
    updateAddress({ ...formData, index: id });
    router.push("/");
    return;
  };

  const handleDelete = () => {
    removeAddress({ index: Number(id) });
    router.push("/");
    return;
  };
  // end: form handle action

  if (!profile) return null;

  return (
    <>
      <MainForm handleSubmit={handleSubmit} editData={profile} />
      <br />
      <Button text="Delete" onClick={handleDelete} />
    </>
  );
};

export default Index;
