import { useState, useEffect } from "react";

import Web3 from "web3";
import detectEthereumProvider from "@metamask/detect-provider";
import { type } from "os";

interface TransactionParams {
  from: string;
  to: string;
  value: string;
}

interface TxFeeParams {
  from: null | string;
  to: null | string;
}

const useMetamask = () => {
  const [account, setAccount] = useState<null | string>(null);
  const [balance, setBalance] = useState<any>(null);
  const [Eth, setEth] = useState<any>(null);
  const [web3, setWeb3] = useState<any>(null);
  const [gas, setGas] = useState(0);
  const [fee, setFee] = useState(0);
  const [chainId, setChainId] = useState(false);
  const [isMeta, setIsMeta] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const init = async () => {
    const provider: any = await detectEthereumProvider();
    if (provider) {
      if (provider !== window.ethereum) {
        console.error("Do you have multiple wallets installed?");
      }
      const chainId = await provider.request({
        method: "eth_chainId",
      });
      setIsMeta(true);
      setChainId(chainId);
      setEth(provider);
      setWeb3(new Web3(provider));
    } else {
      console.log("Please install MetaMask!");
    }
  };

  const connect = () => {
    Eth.request({ method: "eth_requestAccounts" })
      .then(setAccountChange)
      .catch((err: any) => {
        if (err.code === 4001) {
          // EIP-1193 userRejectedRequest error
          // If this happens, the user rejected the connection request.
          console.log("Please connect to MetaMask.");
          setAccount(null);
          setLoading(false);
        } else {
          console.error(err);
        }
      });
  };

  const getAccount = () => {
    Eth.request({ method: "eth_accounts" })
      .then(setAccountChange)
      .catch((err: any) => {
        // Some unexpected error.
        // For backwards compatibility reasons, if no accounts are available,
        // eth_accounts will return an empty array.
        console.error(err, "err");
      });
    Eth.on("accountsChanged", setAccountChange);
  };

  const setAccountChange = async (accounts: Array<string>) => {
    if (accounts.length === 0) {
      console.log("Please connect to MetaMask.");
      setAccount(null);
      setLoading(false);
    } else if (accounts[0] !== account) {
      setAccount(accounts[0]);
      setLoading(false);
    }
  };

  const getBalance = async () => {
    const wei = await web3.eth.getBalance(account);
    if (wei) {
      const ether = web3.utils.fromWei(wei);
      setBalance(ether);
    }
  };

  const getGas = async () => {
    const gasPrice = await web3.eth.getGasPrice(); // estimate the gas price
    setGas(gasPrice);
  };

  const transactionFee = ({ from, to }: TxFeeParams) => {
    const gasLimit = web3.eth.estimateGas({ from, to, gasPrice: gas });
    const fee = gas * gasLimit;
    setFee(fee);
  };

  const transactionSend = async ({ from, to, value }: TransactionParams) => {
    const params = {
      //   nonce: "0x00", // ignored by MetaMask
      //   gasPrice: "0x09184e72a000", // customizable by user during MetaMask confirmation.
      //   gas: "0x2710", // customizable by user during MetaMask confirmation.
      to, // Required except during contract publications.
      from, // must match user's active address.
      value, // Only required to send ether to the recipient from the initiating external account.
      //   data: "0x7f7465737432000000000000000000000000000000000000000000000000000000600057", // Optional, but used for defining smart contract creation and interaction.
      chainId, // Used to prevent transaction reuse across blockchains. Auto-filled by MetaMask.
    };
    try {
      const txHash = await Eth.request({
        method: "eth_sendTransaction",
        params: [params],
      });
      return { data: txHash, error: false };
    } catch (e) {
      return { data: false, error: e };
    }
  };

  const getReload = (_chaindId: string) => {
    console.log("reload page chain changed");
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      if (!Eth) init();
      if (Eth) getAccount();
    }
    return () => {
      ignore = true;
    };
  }, [Eth]);

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      if (account) {
        getBalance();
        getGas();
      }
    }
    return () => {
      ignore = true;
    };
  }, [account]);

  return {
    metaConnect: connect,
    metaAccount: account,
    metaBalance: balance,
    metaGas: gas,
    metaFee: fee,
    metaTransactionFee: transactionFee,
    metaTransactionSend: transactionSend,
    metaStatus: isMeta,
    metaChain: chainId,
    metaLoading: loading,
    metaError: error,
  };
};

export default useMetamask;
