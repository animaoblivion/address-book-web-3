import Rules from "@/libraries/valueValidator/rules";
import { useState } from "react";

interface useFormWrappParams {
  formConfig: any;
}

interface SetNewValidationParams {
  fieldName: string;
  fieldValue: string;
}

const useFormValidation = ({ formConfig }: useFormWrappParams) => {
  const SetNewValidation = ({
    fieldName,
    fieldValue,
  }: SetNewValidationParams) => {
    let validationResponse: Array<string> = [];
    const rules = formConfig[fieldName].rule;
    rules.map((rule: string) => {
      const ruleName = rule as keyof typeof Rules;
      const validationFunction = Rules[ruleName];
      const isValidationFail = validationFunction({ value: fieldValue });
      if (isValidationFail) validationResponse.push(isValidationFail);
    });

    return validationResponse;
  };

  return {
    SetNewValidation,
  };
};

export default useFormValidation;
