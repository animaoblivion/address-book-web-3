import { useState, useEffect } from "react";
import detectEthereumProvider from "@metamask/detect-provider";

interface Params {
  name: string;
  address: string;
  initial: string;
}

interface RemoveParams {
  index: number;
}

interface UpdateParams {
  index: number;
  name: string;
  address: string;
}

const useAddressBook = () => {
  const [contact, setContact] = useState<Params[]>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const init = async () => {
    const addressBook = localStorage.getItem("addressBook");
    if (addressBook) setContact(JSON.parse(addressBook));
  };

  const setLS = () => {
    if (contact.length > 0)
      localStorage.setItem("addressBook", JSON.stringify(contact));
  };

  const add = ({ name, address }: Params) => {
    const a = name.split(" ");
    const initial = a.map((x) => x[0].toUpperCase()).join("");
    const updatedContact = [...contact, { name, address, initial }];
    setContact(updatedContact);
  };

  const remove = ({ index }: RemoveParams) => {
    contact.splice(index, 1);
    const updatedContact = [...contact];
    setContact(updatedContact);
  };

  const update = ({ index, name, address }: UpdateParams) => {
    contact[index].name = name;
    contact[index].address = address;
    const updatedContact = [...contact];
    setContact(updatedContact);
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      setLS();
    }
    return () => {
      ignore = true;
    };
  }, [contact]);

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      init();
    }
    return () => {
      ignore = true;
    };
  }, []);

  return {
    addressBookContact: contact,
    addAddress: add,
    removeAddress: remove,
    updateAddress: update,
    metaLoading: loading,
    metaError: error,
  };
};

export default useAddressBook;
