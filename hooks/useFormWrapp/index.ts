import { useEffect, useState } from "react";

interface useFormWrappParams {
  formConfig: any;
}

interface SetNewDataParams {
  fieldName: string;
  fieldValue: string;
}

interface SetNewErrorParams {
  fieldName: string;
  fieldValue: Array<string>;
}

interface DynamicKeyValObj {
  [key: string]: any;
}

const useFormWrapp = ({ formConfig }: useFormWrappParams) => {
  let formattedOption: DynamicKeyValObj = {};
  let formattedLabel: DynamicKeyValObj = {};
  let formattedData: DynamicKeyValObj = {};
  let formatttedError: DynamicKeyValObj = {};
  Object.keys(formConfig).map((field) => {
    const key = field as keyof typeof formConfig;
    formattedData = { ...formattedData, [key]: formConfig[key]?.value };
    formatttedError = { ...formatttedError, [key]: formConfig[key]?.error };
    // label
    formattedLabel = {
      ...formattedLabel,
      [key]: { label: formConfig[key]?.label, name: key },
    };
    // option
    formattedOption = {
      ...formattedOption,
      [key]: formConfig[key]?.options ? formConfig[key]?.options : [],
    };
  });

  const [option, setOption] = useState<any>(formattedOption);
  const [label, setLabel] = useState<any>(formattedLabel);
  const [data, setData] = useState<any>(formattedData);
  const [error, setError] = useState<any>(formatttedError);
  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    checkHasError();
    return () => {
      null;
    };
  }, [error]);

  useEffect(() => {
    console.log("change and formconfig");
  }, [formConfig]);

  const checkHasError = () => {
    let errs: string[] = [];
    Object.keys(error).map((d) => {
      error[d].map((e: string) => {
        errs = [...errs, e];
      });
    });
    if (errs.length > 0) return setHasError(true);
    return setHasError(false);
  };

  const setNewData = ({ fieldName, fieldValue }: SetNewDataParams) => {
    setData({ ...data, [fieldName]: fieldValue });
  };

  const setBulkData = (newObj: DynamicKeyValObj) => {
    setData({ ...data, ...newObj });
  };

  const setNewError = ({ fieldName, fieldValue }: SetNewErrorParams) => {
    setError({ ...error, [fieldName]: fieldValue });
  };

  const setBulkError = (newObj: DynamicKeyValObj) => {
    setError({ ...error, ...newObj });
  };

  const assignDefault = (newData: DynamicKeyValObj) => {
    const initData: DynamicKeyValObj = {};
    Object.keys(formConfig).map((key: string) => {
      const _getKeyValue_ = (key: string) => (obj: Record<string, any>) =>
        obj[key];
      const objFound = _getKeyValue_(key)(formConfig);
      if (objFound.options && objFound.options.length > 0) {
        initData[key] = objFound.options[0].value;
      } else if (objFound.initial && objFound.initial.length > 0) {
        initData[key] = objFound.initial;
      }
    });
    // assign pre loaded data from server
    const dataFromServer: DynamicKeyValObj = { ...newData };
    Object.keys(dataFromServer).map((key: string) => {
      const _getKeyValue_ = (key: string) => (obj: Record<string, any>) =>
        obj[key];
      const objFound = _getKeyValue_(key)(dataFromServer);
      console.log(objFound);
      if (objFound) {
        initData[key] = objFound;
      }
    });
    setBulkData(initData);
  };

  return {
    formHasError: hasError,
    formOption: option,
    formLabel: label,
    formData: data,
    formError: error,
    assignDefault,
    setNewData,
    setBulkData,
    setNewError,
    setBulkError,
  };
};

export default useFormWrapp;
