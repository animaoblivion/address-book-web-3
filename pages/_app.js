import "../styles/globals.css";
import "tailwindcss/tailwind.css";

import ContextApp from "@/context/index";

function MyApp({ Component, pageProps }) {
  return (
    <ContextApp.Provider>
      <Component {...pageProps} />
    </ContextApp.Provider>
  );
}

export default MyApp;
