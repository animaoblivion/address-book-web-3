import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";

import OptionHeader from "@/components/option";
import ContactProfile from "@/components/contact/profile";
import ContactFee from "@/components/contact/fee";
import FormNewTransaction from "@/components/form-new-transaction";
import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

const Home: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const { addressBookContact } = useAddressBook();
  const profile = addressBookContact[Number(id)];

  const {
    metaConnect,
    metaAccount,
    metaBalance,
    metaGas,
    metaFee,
    metaTransactionFee,
    metaChain,
    metaStatus,
    metaLoading,
  } = useMetamask();

  const handleIsAuth = () => {
    console.log(metaAccount, metaLoading, " metaAccount");
    if (metaAccount === null && !metaLoading) router.push("/login");
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      handleIsAuth();
    }
    return () => {
      ignore = true;
    };
  });

  useEffect(() => {
    if (profile) {
      metaTransactionFee({
        from: metaAccount,
        to: profile.address,
      });
    }
  }, []);

  console.log(metaGas, metaFee, " metaGas");
  if (!profile) return null;

  return (
    <div className="container mx-auto">
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          <OptionHeader
            backLink="/"
            pageTitle={profile.name}
            action={{ actionLink: "/contact-edit/" + id, actionText: "Edit" }}
          />
          <br />

          <ContactProfile
            name={profile.name}
            address={profile.address}
            initial={profile.initial}
          />
          <ContactFee fee={metaFee} />
          <FormNewTransaction address={profile.address} />
        </div>
      </div>
    </div>
  );
};

export default Home;
