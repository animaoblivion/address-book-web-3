import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import OptionHeader from "@/components/option";
import FormNewContact from "@/components/form-new-contact";
import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

const Home: React.FC = () => {
  const router = useRouter();
  const {
    metaConnect,
    metaAccount,
    metaBalance,
    metaChain,
    metaStatus,
    metaLoading,
  } = useMetamask();

  const handleIsAuth = () => {
    console.log(metaAccount, metaLoading, " metaAccount");
    if (metaAccount === null && !metaLoading) router.push("/login");
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      handleIsAuth();
    }
    return () => {
      ignore = true;
    };
  });

  return (
    <div className="container mx-auto">
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          <OptionHeader
            backLink={"/"}
            pageTitle="New Contact"
            action={{ actionLink: undefined, actionText: undefined }}
          />
          <br />

          <FormNewContact />
        </div>
      </div>
    </div>
  );
};

export default Home;
