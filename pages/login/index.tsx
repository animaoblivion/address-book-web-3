import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import ContactItem from "@/components/contact/item";
import Button, { ButtonBlue } from "@/components/misc/button";
import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

const Home: React.FC = () => {
  const router = useRouter();
  const {
    metaConnect,
    metaAccount,
    metaBalance,
    metaChain,
    metaStatus,
    metaLoading,
  } = useMetamask();
  // const isConnecting = !metaAccount && metaLoading ? true : false;

  const handleIsAuth = () => {
    console.log(metaAccount, metaLoading, " metaAccount");
    if (metaAccount !== null && !metaLoading) router.push("/");
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      if (metaAccount) router.push("/");
    }
    return () => {
      ignore = true;
    };
  });

  return (
    <div className="container mx-auto">
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          {metaStatus && <p>metamask available {metaChain}</p>}
          {!metaStatus && <p>metamask not available</p>}
          <Button onClick={metaConnect} text="Connect Wallet" />
        </div>
      </div>
    </div>
  );
};

export default Home;
