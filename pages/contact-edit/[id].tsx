import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import OptionHeader from "@/components/option";
import FormEditContact from "@/components/form-edit-contact";
import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

const Home: React.FC = () => {
  const router = useRouter();
  const { id } = router.query;
  const { addressBookContact } = useAddressBook();
  const profile = addressBookContact[Number(id)];

  const {
    metaConnect,
    metaAccount,
    metaBalance,
    metaChain,
    metaStatus,
    metaLoading,
  } = useMetamask();

  const handleIsAuth = () => {
    console.log(metaAccount, metaLoading, " metaAccount");
    if (metaAccount === null && !metaLoading) router.push("/login");
  };

  useEffect(() => {
    let ignore = false;
    if (!ignore) {
      handleIsAuth();
    }
    return () => {
      ignore = true;
    };
  });

  if (!profile) return null;

  return (
    <div className="container mx-auto">
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          <OptionHeader
            backLink={"/contact-send/" + id}
            pageTitle="Edit"
            action={{ actionLink: undefined, actionText: undefined }}
          />
          <br />

          <FormEditContact />
        </div>
      </div>
    </div>
  );
};

export default Home;
