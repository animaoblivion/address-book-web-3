import { useEffect, useState } from "react";
import Link from "next/link";

import OptionHeader from "@/components/option";
import ContactItem from "@/components/contact/item";
import Button, { ButtonBlue } from "@/components/misc/button";
import useMetamask from "@/hooks/useMetamask";
import useAddressBook from "@/hooks/useAddressBook";

interface AddParams {
  name: string;
  address: string;
}

const Home: React.FC = () => {
  const { addressBookContact, addAddress, removeAddress, updateAddress } =
    useAddressBook();
  const {
    metaConnect,
    metaAccount,
    metaBalance,
    metaChain,
    metaStatus,
    metaLoading,
  } = useMetamask();

  return (
    <div className="container mx-auto">
      <div className="flex justify-center">
        <div className="w-full max-w-xs">
          <OptionHeader
            backLink="/login"
            pageTitle={""}
            action={{ actionLink: undefined, actionText: undefined }}
          />
          <br />

          <Link href="/contact-new">
            <a> + New Contact</a>
          </Link>
          <br />
          <br />

          {addressBookContact.map((ab, index) => {
            return (
              <div key={index}>
                <Link href={"/contact-send/" + index}>
                  <a>
                    <ContactItem name={ab.name} initial={ab.initial} />
                  </a>
                </Link>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Home;
